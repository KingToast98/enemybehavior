﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    public float lookRadius = 3f;
    public Transform target;

    NavMeshAgent agent;

    public float timeBetweenShots;
    public float startTimeBetweenShots;
    public GameObject projectile;

    public float speed;
    public float stoppingDistance;
    public float retreatDistance;

    public Transform player;

    public int level = 1;

   // PlayerMovement playMove;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        float distance = Vector2.Distance(target.position, transform.position);
       Debug.Log(distance);
        if (distance <= lookRadius)
        {
            if(PlayerMovement.instance.level < level)
            {
                Debug.Log("Back of");
                Chase();
            }
            else if(PlayerMovement.instance.level > level)
            {
                RunAway();

            }

        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);

    }

    void Attack()
    {
        Instantiate(projectile, transform.position, Quaternion.identity);
    }

    void Chase()
    {
        if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
            Attack();
        }

       
    }

    void RunAway()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
        Debug.Log("Ishouldrunaway");
    }
}
